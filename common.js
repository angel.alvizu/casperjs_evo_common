var commonLib = function() {
	this.captureJpg = function(context, outputImage) {
	    context.capture(outputImage, undefined, {
	        format: 'jpg',
	        quality: 40
	    });
	}
};

module.exports = new commonLib;